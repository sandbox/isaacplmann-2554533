<?php
/**
 * @file
 * Contains test cases for the Video Embed Columbus Dispatch module.
 */

namespace Drupal\Tests\video_embed_columbus_dispatch\Unit;

/**
 * Tests embedding videos from the Columbus Dispatch website.
 *
 * @group video_embed_columbus_dispatch
 */
class VideoEmbedColumbusDispatchTest extends UnitTestCase {
  /**
   * Tests the functionality of the module.
   */
  public function testVideoEmbedColumbusDispatch() {
    $url = "http://www.dispatch.com/content/mediaplayer/embed.html?ooid=pzNTIydzrBATDwjPaVT5dMyU6Cchkn6H&cmpid=share";
    $parameters = _video_embed_columbus_dispatch_get_video_properties($url);
    $this->assertArrayHasKey("id", $parameters);
    $this->assertEquals($parameters['id'], 'pzNTIydzrBATDwjPaVT5dMyU6Cchkn6H');
  }

}
