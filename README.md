CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
The Video Embed Columbus Dispatch module allows videos from the Columbus
Dispatch website (dispatch.com) to be embedded in Video Embed fields using
the Video Embed module.


 * For a full description of the module, visit the project page:
   https://drupal.org/project/video_embed_columbus_dispatch


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/video_embed_columbus_dispatch


REQUIREMENTS
------------
This module requires the following modules:
 * Video Embed (https://drupal.org/project/video_embed_field)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

   - Administer video styles (Video Embed Field)

     You will need this permission enabled in order to customize the size of the
     embedded video.

 * Customize the embedded videos in Administration » Configuration »
   Media » Video Embed styles.


USAGE
-----
 1 Add a Video Embed field to a content type
 2 When adding or editing a node, copy the link from the iframe code on the
   dispatch video. The format should look like this:

    http://www.dispatch.com/content/mediaplayer/embed.html?ooid=pzNTIydzrBATDwjPaVT5dMyU6Cchkn6H&cmpid=share


MAINTAINERS
-----------
Current maintainers:
 * Isaac Mann (isaacplmann) - https://www.drupal.org/u/isaacplmann
